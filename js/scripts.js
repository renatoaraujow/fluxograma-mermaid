var config = {
    startOnLoad: true,
    flowchart: {
        useMaxWidth: false,
        htmlLabels: true,
        curve: 'cardinal',
    },
    securityLevel: 'loose',
    theme: 'base',
    themeVariables: {
        primaryColor: '#FFFFFF',
        darkMode: false,
        background: '#FFFFFF',
        fontFamily: 'Tahoma',
        primaryBorderColor: '#191847',
        primaryTextColor: '#191847',
    },
    logLevel: 1
};

mermaid.initialize(config);

var callback = function() {
    alert('A callback was triggered');
}

function goToByScroll(el) {
    $('html,body').animate({
        scrollTop: $(el).offset().top - $(el)[0].getBoundingClientRect().height,
        scrollLeft: $(el).offset().left - $(el)[0].getBoundingClientRect().width,
    }, 'slow');
}

function addBreakLine(textWithoutBreaks, numberCharacters) {
    let breakLine = false;

    const textWithBreaks = textWithoutBreaks.split('')
        .reduce((letters, letter, index) => {
            if (index && (index + 1) % numberCharacters === 1) {
                if (letter === ' ') {
                    return [...letters, "</br>", letter]
                } else {
                    breakLine = true;
                    return [...letters, letter]
                }
            } else {
                if (breakLine === true && letter === ' ') {
                    breakLine = false;
                    return [...letters, "</br>", letter]
                } else {
                    return [...letters, letter]
                }
            }

        }, []).join('');

    return textWithBreaks;
}

jQuery(function() {
    $("[class*=child]").hide(0).css('visibility', 'visible');
    $(".edgePath").hide(0).css('visibility', 'visible');
    $(".loading").fadeOut(500);
    $(".mermaid").hide(0).css('visibility', 'visible').fadeIn(500);
    $("header").fadeIn(500);

    $.fn.hasPartialAttr = function(partial, attr = 'class') {
        return new RegExp(partial).test(this.attr(attr));
    };

    $.fn.getOptionNumber = function() {
        return this.attr('class').split(' ').filter(x => new RegExp('dad').test(x)).toString().replace(/[^0-9]/gi, '');
    }

    function arrowToggle(elementId, event) {
        $('.edgePath').each(function() {
            if ($(this).attr('id').split('-')[1] === `${elementId}`) {
                if (event === 'show') {
                    $(this).fadeIn(500);
                } else if (event === 'hide') {
                    $(this).fadeOut(500);
                }
            }
        });
    }

    $('.node').each(function() {
        let nodeId = $(this).attr('id').split('-')[1];

        if (!$(this).hasPartialAttr('dad') && !$(this).hasPartialAttr('child')) {
            arrowToggle(nodeId, 'show');
        }

        if (!$(`.edgePath.LS-${nodeId}`).length) {
            $(this).addClass('endNode');
        }
    });

    let openedChildNodes = [];

    function openNode(childNodeElements, childNodeName, dadId, recursive = true) {

        // mostra o filho
        childNodeElements.fadeIn(500);
        openedChildNodes.push(childNodeName);
        arrowToggle(dadId, 'show');

        // abre as setas dos filhos
        childNodeElements.each(function() {
            const childNodeElement = $(this)
            if (!childNodeElement.hasPartialAttr('dad')) {
                let childId = childNodeElement.attr('id').split('-')[1];
                arrowToggle(childId, 'show');
            }
        });

        // abrir todos recursivamente
        if (recursive) {
            childNodeElements.each(function() {
                const childNodeElement = $(this)
                if (childNodeElement.hasPartialAttr('dad')) {

                    childNodeName = `option-child-${childNodeElement.getOptionNumber()}`
                    childNodeElements = $(`g.${childNodeName}`);
                    const dadId = childNodeElement.attr('id').split('-')[1];

                    openNode(childNodeElements, childNodeName, dadId, recursive);
                }
            })
        }
    }


    function closeRecursive(childNodeElements, childNodeName, dadId) {

        // fecha ele e suas setas
        childNodeElements.fadeOut(500);
        openedChildNodes = openedChildNodes.filter(e => e !== childNodeName);
        arrowToggle(dadId, 'hide');

        // fecha suas setas
        childNodeElements.each(function() {
            const childNodeElement = $(this)
            if (!childNodeElement.hasPartialAttr('dad')) {
                let childId = childNodeElement.attr('id').split('-')[1];
                arrowToggle(childId, 'hide');
            }
        });

        // repete a operação de fechar
        childNodeElements.each(function() {
            const childNodeElement = $(this)
            if (childNodeElement.hasPartialAttr('dad')) {

                childNodeName = `option-child-${childNodeElement.getOptionNumber()}`
                childNodeElements = $(`g.${childNodeName}`);
                const dadId = childNodeElement.attr('id').split('-')[1];

                closeRecursive(childNodeElements, childNodeName, dadId);
            }
        })
    }

    $("g.nodes").on("click", "> g", function() {

        const clickedNode = $(this);

        if (clickedNode.hasPartialAttr('dad')) {
            goToByScroll(clickedNode);

            const childNodeName = `option-child-${clickedNode.getOptionNumber()}`;
            const childNodeElements = $(`g.${childNodeName}`);
            const dadId = clickedNode.attr('id').split('-')[1];

            if (openedChildNodes.indexOf(childNodeName) === -1) { // se o filho não estiver aberto
                openNode(childNodeElements, childNodeName, dadId, false) // abre ele (se recursivo = true, ele vai abrir tambe os filhos dele)
            } else { // se o filho estiver aberto             
                closeRecursive(childNodeElements, childNodeName, dadId); // fecha o nó e seus filhos recursivamente
            }
        }
    });
});